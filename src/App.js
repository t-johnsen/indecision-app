// @flow
import React, {Component} from "react";
/*
import {hot} from "react-hot-loader";
*/
import "./App.css";

type HeaderProps = { title: string, subtitle: string };

export const Header = (props: HeaderProps) => {
	
	return (
		<div className = "App">
			<h1>{props.title}</h1>
			<h2>{props.subtitle}</h2>
		</div>
	);
	
};

type ActionProps = { hasOptions: boolean, handlePick: mixed };

export const Action = (props: ActionProps) => {
	
	return (
		<div>
			<button
				onClick = {props.handlePick}
				disabled = {!props.hasOptions}
			>
				What should I do?
			</button>
		</div>
	);
	
	
};


type OptionsProps = { options: string[], handleDeleteOptions: mixed };

export const Options = (props: OptionsProps) => {
	
	return (
		<div>
			<button onClick = {props.handleDeleteOptions}>Remove all options</button>
			{
				props.options.map((option, i) => <Option key = {i} optionText = {option} />)
			}
		</div>
	)
	
};

export const Option = (props: { optionText: string }) => {
	return (
		<div>
			{props.optionText}
		</div>
	)
};

type AddOptionProps = { /*handleAddOption(option: string): () => mixed,*/ error: string | typeof undefined };

export class AddOption extends Component<*, AddOptionProps> {
	
	constructor (props: AddOptionProps) {
		super(props);
		this.handleAddOptionHere = this.handleAddOptionHere.bind(this);
		this.state = {
			error: undefined
		}
	}
	
	// $FlowFixMe
	handleAddOptionHere = (e) => {
		e.preventDefault();
		
		const option = e.target.elements.option.value.trim();
		const error = this.props.handleAddOption(option);
		
		this.setState(() => {
			return {
				error: error
			}
		});
		
	};
	
	
	render () {
		return (
			<div>
				{this.state.error && <p>{this.state.error}</p>}
				<form onSubmit = {this.handleAddOptionHere}>
					<input type = 'text' name = 'option' />
					
					<button>Add option</button>
				
				</form>
			</div>
		)
	};
	
}


/*export default hot(module)(Header, Action); -- doesnt work*/
