// @flow
import React, {Component} from "react";
import ReactDOM from "react-dom";
import {Header, Action, Options, AddOption} from "./App.js";

type Props = { options: string[] };

class IndecisionApp extends Component<*, Props> {
	
	constructor (props: Props) {
		super(props);
		// $FlowFixMe
		this.handleDeleteOptions = this.handleDeleteOptions.bind(this);
		// $FlowFixMe
		this.handlePick = this.handlePick.bind(this);
		// $FlowFixMe
		this.handleAddOption = this.handleAddOption.bind(this);
		this.state = {
			options: []
		}
	}
	
	handleDeleteOptions () {
		this.setState(() => {
			return {
				options: []
			};
		});
	}
	
	handlePick () {
		const randomNum = Math.floor(Math.random() * this.state.options.length);
		const option = this.state.options[randomNum];
		alert(option);
	}
	
	handleAddOption (option: string) {
		
		if (!option) {
			return 'Enter valid value to add value';
		} else if (this.state.options.indexOf(option) > -1) {
			return 'This option already exists';
		}
		
		this.setState((prevState) => {
			return {
				options: prevState.options.concat([option])
			};
		});
	}
	
	render () {
		
		const title: string = 'Indecision';
		const subtitle: string = 'Put your life in the hand of a computer';
		
		return (
			<div>
				<Header title = {title} subtitle = {subtitle} />
				<Action
					hasOptions = {this.state.options.length > 0}
					handlePick = {this.handlePick}
				/>
				<Options
					options = {this.state.options}
					handleDeleteOptions = {this.handleDeleteOptions}
				/>
				<AddOption
					handleAddOption = {this.handleAddOption}
				/>
			</div>
		)
	};
	
}

const root = document.getElementById("root");
if (root) {
	ReactDOM.render(
		<IndecisionApp />,
		root
	)
}